package simultaneously.client;

import com.google.gwt.event.shared.*;
import com.google.gwt.user.client.ui.*;

import simultaneously.client.events.*;
import simultaneously.client.presenter.*;
import simultaneously.client.presenter.MainPagePresenter.*;
import simultaneously.client.views.*;

public class AppController {
    HandlerManager eventBus;
    LoginPresenter loginPage;
    HasWidgets container;

    public AppController (HandlerManager manager) {
	this.eventBus = manager;
	loginPage = new LoginPresenter (new LoginView (), eventBus);
	bindEvents ();
    }

    public void bindEvents () {
	eventBus.addHandler (LoginEvent.TYPE, new LoginEventHandler () {
	    @Override public void onLogin (LoginEvent event) {
		MainPagePresenter mainpage = new MainPagePresenter ((Display) new MainPageView (), eventBus);
		container = mainpage.getView ().getViewInstance ();
		mainpage.go (RootPanel.get ());
	    }
	});

	eventBus.addHandler (LogoutEvent.TYPE, new LogoutEventHandler () {
	    @Override public void onLogout (LogoutEvent event) {
		loginPage.go (RootPanel.get ());
	    }
	});
    }

    public void goTo (HasWidgets page) {
	this.container = page;
	loginPage.go (page);
    }

}
