package simultaneously.client.views;

import java.util.*;
import simultaneously.client.presenter.*;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.ui.*;

public class LoginView implements HasWidgets, LoginPresenter.Display {
    HorizontalPanel container;
    Label loginLabel;
    Label passwordLabel;
    TextBox loginField;
    PasswordTextBox passwordField;
    Button loginButton;

    public LoginView () {
	container = new HorizontalPanel ();
	loginField = new TextBox ();
	loginButton = new Button ("Login");
	passwordField = new PasswordTextBox ();
	loginLabel = new Label ("Login");
	passwordLabel = new Label ("Password");

	container.add (loginLabel);
	container.add (loginField);
	container.add (passwordLabel);
	container.add (passwordField);
	container.add (loginButton);
    }

    @Override public Widget asWidget () {
	return container;
    }

    @Override public void add (Widget w) {
	container.add (w);
    }

    @Override public void clear () {
	container.clear ();
    }

    @Override public Iterator<Widget> iterator () {
	return container.iterator ();
    }

    @Override public boolean remove (Widget w) {
	return container.remove (w);
    }

    @Override public HasClickHandlers getLoginButton () {
	return loginButton;
    }

    @Override public LoginView getViewInstance () {
	return this;
    } 

}
