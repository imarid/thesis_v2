package simultaneously.server;

import java.io.*;
import java.util.*;

import com.google.gwt.user.client.rpc.*;

@RemoteServiceRelativePath ("accessservice") public interface AccessFilesService extends RemoteService {
    public List<File> getFiles ();

    public Long saveFile (File file);

}
