package simultaneously.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.shared.*;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Thesis_v2 implements EntryPoint {

    public void onModuleLoad () {
	HandlerManager eventBus = new HandlerManager (null);
	AppController app = new AppController (eventBus);
	app.goTo (RootPanel.get ());
    }
}
