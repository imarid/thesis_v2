package simultaneously.client.model;

import java.io.*;
import java.util.*;

public class UserProfile implements Serializable {
    Long id;
    String name;
    String password;
    Set<File> grants;
}
