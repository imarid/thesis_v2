package simultaneously;

import simultaneously.client.Thesis_v2Test;
import com.google.gwt.junit.tools.GWTTestSuite;
import junit.framework.Test;
import junit.framework.TestSuite;

public class Thesis_v2Suite extends GWTTestSuite {
    public static Test suite () {
	TestSuite suite = new TestSuite ("Tests for Thesis_v2");
	suite.addTestSuite (Thesis_v2Test.class);
	return suite;
    }
}
