package simultaneously.client.views;

import java.util.*;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.ui.*;

import simultaneously.client.presenter.*;

public class RegisterView implements HasWidgets, RegisterPresenter.Display {
    HorizontalPanel container;
    Label loginLabel;
    Label passwordLabel;
    Label repeatPasswordLabel;
    TextBox loginField;
    PasswordTextBox passwordField;
    PasswordTextBox repeatPasswordField;
    Button registerButton;

    public RegisterView () {
	container = new HorizontalPanel ();
	loginField = new TextBox ();
	registerButton = new Button ("Register");
	passwordField = new PasswordTextBox ();
	repeatPasswordField = new PasswordTextBox ();
	loginLabel = new Label ("Login");
	passwordLabel = new Label ("Password");
	repeatPasswordLabel = new Label ("Repeated password");

	container.add (loginLabel);
	container.add (loginField);
	container.add (passwordLabel);
	container.add (passwordField);
	container.add (registerButton);
    }
    @Override public HasClickHandlers getRegisterButton () {
	return registerButton;
    }

    @Override public Widget asWidget () {
	return container;
    }

    @Override public void add (Widget w) {
	container.add (w);
    }

    @Override public void clear () {
	container.clear ();
    }

    @Override public Iterator<Widget> iterator () {
	return container.iterator ();
    }

    @Override public boolean remove (Widget w) {
	return container.remove (w);
    }
    @Override public RegisterView getViewInstance () {
	return this;
    }
}
