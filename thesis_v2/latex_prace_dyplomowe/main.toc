\contentsline {subsection}{\numberline {0.0.1}{Zawarto\IeC {\'s}\IeC {\'c} merytoryczna}}{5}% 
\contentsline {section}{\numberline {0.1}{S\IeC {\l }ownik poj\IeC {\k e}\IeC {\'c}}}{5}% 
\contentsline {subsection}{\numberline {0.1.1}{Aplikacje typu RIA}}{5}% 
\contentsline {subsection}{\numberline {0.1.2}{Java}}{5}% 
\contentsline {subsection}{\numberline {0.1.3}{System zarz\IeC {\k a}dzania baz\IeC {\k a} danych, baza danych}}{5}% 
\contentsline {subsection}{\numberline {0.1.4}{Framework albo platforma programistyczna}}{5}% 
\contentsline {subsection}{\numberline {0.1.5}{Google Web Toolkit}}{6}% 
\contentsline {subsection}{\numberline {0.1.6}{JavaScript, a w\IeC {\l }a\IeC {\'s}ciwie ECMAScript}}{6}% 
\contentsline {subsection}{\numberline {0.1.7}{XHR}}{6}% 
\contentsline {subsection}{\numberline {0.1.8}{Chmura obliczeniowa}}{6}% 
\contentsline {subsection}{\numberline {0.1.9}{SaaS, z ang. oprogramowanie jako us\IeC {\l }uga}}{6}% 
\contentsline {subsection}{\numberline {0.1.10}{Debuggowanie i tryb debug}}{6}% 
\contentsline {chapter}{\numberline {1}{Przegl\IeC {\k a}d istniej\IeC {\k a}cych rozwi\IeC {\k a}za\IeC {\'n} do jednoczesnej pracy na dokumentami tekstowymi}}{7}% 
\contentsline {section}{\numberline {1.1}{Istniej\IeC {\k a}ce obecnie na rynku plikacje RIA do wsp\IeC {\'o}lnej pracy nad dokumentami}}{7}% 
\contentsline {subsection}{\numberline {1.1.1}{Google Docs}}{7}% 
\contentsline {subsection}{\numberline {1.1.2}{Microsoft Word Online}}{8}% 
\contentsline {subsection}{\numberline {1.1.3}{Dropbox Paper}}{8}% 
\contentsline {subsection}{\numberline {1.1.4}{Etherpad Lite}}{9}% 
\contentsline {section}{\numberline {1.2}\leavevmode {\color {BrickRed}Klasyfikacja metod literaturowych}}{10}% 
\contentsline {subsection}{\numberline {1.2.1}\leavevmode {\color {BrickRed}Opis metody literaturowej 1}}{10}% 
\contentsline {subsection}{\numberline {1.2.2}\leavevmode {\color {BrickRed}Cytowania}}{10}% 
\contentsline {subsection}{\numberline {1.2.3}\leavevmode {\color {BrickRed}Jak zamieszcza\IeC {\'c} rysunki?}}{10}% 
\contentsline {subsection}{\numberline {1.2.4}\leavevmode {\color {BrickRed}Jak formatowa\IeC {\'c} tekst?}}{11}% 
\contentsline {subsection}{\numberline {1.2.5}\leavevmode {\color {BrickRed}Przyk\IeC {\l }ad zamieszczania wzor\IeC {\'o}w}}{11}% 
\contentsline {subsection}{\numberline {1.2.6}\leavevmode {\color {BrickRed}Podsumowania}}{11}% 
\contentsline {chapter}{\numberline {2}{Narz\IeC {\k e}dzie, technologie i metody u\IeC {\.z}yte na potrzeby projektu zwi\IeC {\k a}zanego z tematem pracy in\IeC {\.z}ynierskiej}}{12}% 
\contentsline {section}{\numberline {2.1}{Framework GWT}}{12}% 
\contentsline {subsection}{\numberline {2.1.1}{Zasada dzia\IeC {\l }ania}}{12}% 
\contentsline {subsection}{\numberline {2.1.2}{Tryb debug}}{13}% 
\contentsline {subsection}{\numberline {2.1.3}{Zastosowania}}{13}% 
\contentsline {subsection}{\numberline {2.1.4}{Przyk\IeC {\l }ady u\IeC {\.z}ycia}}{13}% 
\contentsline {section}{\numberline {2.2}{J\IeC {\k e}zyk programowania}}{13}% 
\contentsline {subsection}{\numberline {2.2.1}{Java 8}}{13}% 
\contentsline {section}{\numberline {2.3}{Przegl\IeC {\k a}d RDBMS}}{14}% 
\contentsline {subsection}{\numberline {2.3.1}{Komunikacja z baz\IeC {\k a} danych - JDBC}}{14}% 
\contentsline {chapter}{\numberline {3}\leavevmode {\color {BrickRed}Projekt systemu do jednoczesnej pracy grupowej}}{15}% 
\contentsline {section}{\numberline {3.1}{Wymagania funkcjonalne}}{15}% 
\contentsline {subsection}{\numberline {3.1.1}Aplikacja klienta}{15}% 
\contentsline {subsection}{\numberline {3.1.2}Aplikacja serwera}{16}% 
\contentsline {subsection}{\numberline {3.1.3}Format danych wej\IeC {\'s}ciowych oraz wyj\IeC {\'s}ciowych}{16}% 
\contentsline {section}{\numberline {3.2}{Diagramy przypadk\IeC {\'o}w u\IeC {\.z}ycia}}{16}% 
\contentsline {section}{\numberline {3.3}{Diagramy sekwencji}}{16}% 
\contentsline {chapter}{\numberline {4}Rezultaty i wnioski}{17}% 
\contentsline {section}{\numberline {4.1}Opis procedury testowej}{17}% 
\contentsline {section}{\numberline {4.2}Prezentacja rezultat\IeC {\'o}w}{17}% 
\contentsline {section}{\numberline {4.3}Wnioski}{18}% 
\contentsline {chapter}{\numberline {A}Dodatek}{22}% 
\contentsline {chapter}{\numberline {B}Opis zawarto\IeC {\'s}ci p\IeC {\l }yty CD}{23}% 
