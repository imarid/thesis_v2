package simultaneously.client.presenter;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.shared.*;
import com.google.gwt.user.client.ui.*;

import simultaneously.client.events.*;
import simultaneously.client.views.*;

public class MainPagePresenter {
    
    public interface Display {
	HasClickHandlers getLogoutButton ();
	Widget asWidget ();
	MainPageView getViewInstance ();
	Button getButton ();
    }

    final Display display;
    final HandlerManager eventBus;

    public MainPagePresenter (Display display, HandlerManager eventBus) {
	this.display = display;
	this.eventBus = eventBus;
    }

    public void init () {
	display.getLogoutButton ().addClickHandler (new ClickHandler () {
	    @Override public void onClick (ClickEvent event) {
		// use the event bus to trigger the event
		eventBus.fireEvent (new LogoutEvent ());
	    }
	});
    }

    public void go (final HasWidgets container) {
	init ();
	container.clear ();
	container.add (display.asWidget ());

    }

    public Display getView () {
	return display;
    }

}
