package simultaneously.client.views;

import java.util.*;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.ui.*;

import simultaneously.client.presenter.*;

public class MainPageView implements HasWidgets, MainPagePresenter.Display {

    VerticalPanel container;
    HorizontalPanel leftPanel;
    HorizontalPanel rightPanel;
    Button logout;
    MainPageView instance;

    public MainPageView () {
	leftPanel = new HorizontalPanel ();
	rightPanel = new HorizontalPanel ();
	container = new VerticalPanel ();
	logout = new Button ("Logout");
	container.add (logout);
	container.add (leftPanel);
	container.add (rightPanel);
    }

    @Override public Widget asWidget () {
	return container;
    }

    @Override public void add (Widget w) {
	container.add (w);
    }

    @Override public void clear () {
	container.clear ();
    }

    @Override public Iterator<Widget> iterator () {
	return container.iterator ();
    }

    @Override public boolean remove (Widget w) {
	return container.remove (w);
    }

    @Override public Button getButton () {
	return logout;
    }

    @Override public HasClickHandlers getLogoutButton () {
	return logout;
    }

    @Override public MainPageView getViewInstance () {
	if (instance == null)
	    return new MainPageView ();
	else
	    return instance;
    }
}
