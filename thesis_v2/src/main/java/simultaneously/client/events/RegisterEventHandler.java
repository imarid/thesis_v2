package simultaneously.client.events;

import com.google.gwt.event.shared.*;

public interface RegisterEventHandler extends EventHandler {

    public void onRegister (RegisterEvent registerEvent);
}
