package simultaneously.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface LoginEventHandler extends EventHandler {
    public void onLogin (LoginEvent event);
}