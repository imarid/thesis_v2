package simultaneously.server;

import java.io.*;
import java.util.*;

import com.google.gwt.user.client.rpc.*;

public interface AccessFilesServiceAsync {
    public void getFiles (AsyncCallback<List<File>> callback);
    
    public void saveFile (File fileDTO, AsyncCallback<Long> callback);

}
