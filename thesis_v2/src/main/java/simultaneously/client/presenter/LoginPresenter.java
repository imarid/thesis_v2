package simultaneously.client.presenter;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.shared.*;
import com.google.gwt.user.client.ui.*;

import simultaneously.client.events.*;
import simultaneously.client.views.*;

public class LoginPresenter {
    public interface Display {
	HasClickHandlers getLoginButton ();
	Widget asWidget ();
	LoginView getViewInstance ();
    }

    final HandlerManager eventBus;
    final Display view;

    public LoginPresenter (Display view, HandlerManager eventBus) {
	this.eventBus = eventBus;
	this.view = view;
    }

    public void bindEvents () {
	view.getLoginButton ().addClickHandler (new ClickHandler () {
	    @Override public void onClick (ClickEvent event) {
		eventBus.fireEvent (new LoginEvent ());
	    }
	});
    }

    public void go (final HasWidgets container) {
	bindEvents ();
	container.clear ();
	container.add (view.getViewInstance ().asWidget ());
    }

    public Display getView () {
	return view;
    }

}
